package fr.univlille.iut.r304.tp4.q1;

public class Chronometer implements Observer {
    int seconde;

    public Chronometer(int seconde) {
        this.seconde = seconde;
    }
    public Chronometer() {
        seconde = 0;
    }

    @Override
    public void update(Subject subj) {
        seconde = seconde + 1;
    }

    @Override
    public void update(Subject subj, Object data) {
        seconde = seconde + 1;
    }
    
    @Override
    public String toString() {
        return "Chronometer [seconde=" + seconde + "]";
    }
    
    public int getSeconde() {
        return seconde;
    }

    
    
}