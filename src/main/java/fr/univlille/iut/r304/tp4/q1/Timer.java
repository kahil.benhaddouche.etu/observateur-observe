package fr.univlille.iut.r304.tp4.q1;

import java.util.ArrayList;

public class Timer extends Subject {
	TimerThread timerThread;

	public Timer(TimerThread timerThread) {
			this.observers = new ArrayList<>();
			this.timerThread = timerThread;
	}

	public Timer() {
		this.observers = new ArrayList<>();
		this.timerThread = new TimerThread(this);
	}

	public void start() {
		this.timerThread.start();
	}

	public void stopRunning() {
		this.timerThread.interrupt();
	}

	public void attach(Observer observer) {
		this.observers.add(observer);
	}

	public void detach(Observer observer) {
		this.observers.remove(observer);
	}

	
}


