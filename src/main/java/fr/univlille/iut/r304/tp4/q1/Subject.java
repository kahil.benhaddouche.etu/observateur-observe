package fr.univlille.iut.r304.tp4.q1;

import java.util.ArrayList;
import java.util.List;

public abstract class Subject {
	protected List<Observer> observers;

	public Subject() {
		this.observers = new ArrayList<>();
	}

	protected void notifyObservers() {
		for (Observer observer : observers) {
			observer.update(this);
		}
	}

	protected void notifyObservers(Object data) {
		for (Observer observer : observers) {
			observer.update(this, data);
		}
	}

	public void attach(Observer observer) {
		this.observers.add(observer) ;
	}

	public void detach(Observer observer) {
		this.observers.remove(observer);
	}
}
