package fr.univlille.iut.r304.tp4.q1;

public class TimerThread extends Thread {
    Timer timer;
    
    TimerThread(Timer timer) {
        this.timer = timer;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(1000);
                timer.notifyObservers();
            } catch (InterruptedException e) {
                // on ignore et on espère que ce n’est pas grave
            }
        }
    }
}